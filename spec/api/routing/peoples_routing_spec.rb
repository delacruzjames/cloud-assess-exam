require "rails_helper"

RSpec.describe Api::PeoplesController, type: :routing do
  describe "routing" do
    it { expect(:get => "api/temporary_people").to route_to("api/peoples#index") }
    it { expect(:get => "api/temporary_people/1").to route_to("api/peoples#show", id: "1") }
  end
end
