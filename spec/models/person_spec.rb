require 'rails_helper'

RSpec.describe Person, type: :model do
  describe "validations" do
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
  end

  describe "associations" do
     it { should belong_to(:organisation) }
  end
end
