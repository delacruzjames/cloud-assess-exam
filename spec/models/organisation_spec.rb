require 'rails_helper'

RSpec.describe Organisation, type: :model do
  describe "validations" do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:location) }
  end

  describe "associations" do
     it { should have_many(:persons) }
  end
end
