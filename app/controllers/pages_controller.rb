class PagesController < ApplicationController
  before_action :authenticate_user!
  def index
    @persons = Person.filter(params.slice(:first_name, :last_name))
  end
end
