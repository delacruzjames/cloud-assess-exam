class Api::PeoplesController < Api::BaseController

  def index
    @persons = Person.filter(params.slice(:first_name, :last_name))
    render json: @persons
  end

  def show
    @person = Person.find(params[:id])
    render json: @person
  end
end
