class Person < ApplicationRecord
  include Filter
  scope :first_name, -> (first_name) { where(first_name: first_name) }
  scope :last_name, -> (last_name) { where(last_name: last_name) }
  # scope :title, -> (title) { self.joins(:organisation).where(organisation: {title: title}) }

  validates :first_name, presence: true
  validates :last_name, presence: true

  belongs_to :organisation
end
