class Organisation < ApplicationRecord
  validates :title, presence: true
  validates :location, presence: true

  has_many :persons
end
