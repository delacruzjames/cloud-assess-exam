class OrganisationSerializer < ApplicationSerializer
  attributes :id, :title, :location
end
