class PersonSerializer < ApplicationSerializer
  attributes :id, :first_name, :last_name
  belongs_to :organisation
end
