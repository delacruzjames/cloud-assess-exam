# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

@organisations = [
  {title: "ABC Company", location: "Sydney"},
  {title: "XYZ Company", location: "Brisbane"},
  {title: "Company ABC XYZ", location: "Melbourne"}
]

@organisations.each do |org_params|
  Organisation.find_or_create_by(org_params)
end

@persons = [
  {first_name: "Paul", last_name: "Smith", organisation_id: 1, assessment_count: 5},
  {first_name: "Andrew", last_name: "Baker", organisation_id: 2, assessment_count: 0},
  {first_name: "Michael", last_name: "Clarke", organisation_id: 3, assessment_count: 14},
  {first_name: "David", last_name: "Cline", organisation_id: 1, assessment_count: 3},
  {first_name: "Adam", last_name: "Boardbent", organisation_id: 1, assessment_count: 3}
]

@persons.each do |person_params|
  Person.find_or_create_by(person_params)
end
