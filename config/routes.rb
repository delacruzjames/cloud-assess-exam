Rails.application.routes.draw do
  devise_for :users
  root "pages#index"

  # API Endpoint
  namespace :api do
    resources :peoples, only: [:index, :show], path: "temporary_people"
  end
end
